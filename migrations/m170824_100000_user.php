<?php

use yii\db\Migration;

/**
 * Class m170824_112506_user
 */
class m170824_100000_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
            $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(128)->notNull()->unique(),
            'password' => $this->string(128)->notNull(),
            'email' => $this->string(128)->notNull()->unique(),
        ]);
        

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170824_112506_user cannot be reverted.\n";

        return false;
    }
    */
}
