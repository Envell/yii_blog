<?php

use yii\db\Migration;

/**
 * Class m170824_104108_post
 */
class m170824_104108_post extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {   
        
        $this->createTable('post', [
            'id' => $this->primaryKey(),
            'title' => $this->string(128)->notNull()->unique(),
            'content' => $this->text()->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'created_by' => $this->integer()->notNull(),
        ]);
        
        $this->createIndex('post_index', 'post', ['created_by']);
        $this->addForeignKey('fk_post_user_created_by', 'post', 'created_by', 'user', 'id', 'CASCADE', 'CASCADE');
 
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('post');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170824_104108_post cannot be reverted.\n";

        return false;
    }
    */
}
