<?php

use yii\db\Migration;

/**
 * Class m170824_120740_comment
 */
class m170824_120740_comment extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'title' => $this->string(128)->notNull()->unique(),
            'content' => $this->text()->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'post_id' => $this->integer()->notNull()
        ]);
        $this->createIndex('comment_index', 'post', ['created_by']);
        $this->addForeignKey('fk_comment_user_created_by', 'comment', 'created_by', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_comment_post_id', 'comment', 'post_id', 'post', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('comment');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170824_120740_comment cannot be reverted.\n";

        return false;
    }
    */
}
